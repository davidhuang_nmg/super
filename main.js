var config = require("./config/config"),
    express = require('express'),
    authService = require("./service/auth"),
    emailService = require("./service/email"),
    customService = require("./service/custom"),
    smsService = require("./service/sms"),
    redisLogService = require("./service/log"),
    schedule = require("node-schedule"),
    kue = require("kue"),
    moment = require("moment"),
    redis = require('redis'),
    mkdirp = require('mkdirp'),
    logger = require("./service/winston"),
    redisClient = redis.createClient(config.redis.master.port, config.redis.master.host),
    rule = new schedule.RecurrenceRule(),
    Thenjs = require('thenjs'),
    _ = require("underscore"),
    times = [],
    taskTypes = config.support_task_type,
    keyType = "server";
    
if (config.memwatch) {
	mkdirp.sync("./tmp");
	var memwatch = require('memwatch'),
		heapdump = require('heapdump');
	memwatch.on('leak', function(info) {
		var file = './tmp/main-' + process.pid + '-' + Date.now() + '.heapsnapshot';
		heapdump.writeSnapshot(file, function(err){
			if (err) console.error(err);
			else console.error('Wrote snapshot: ' + file);
		});
	});
}

redisClient.auth(config.redis.master.password, function (err,authResp) {
	if (err) {
		throw new Error('Redis client Error ' + err + ",Resp:" + authResp);
	}
});
redisClient.select(config.redis.master.db_index, function (err, res) {
	if (err) throw new Error('Select Redis DB Error ' + err);
});

redisClient.on('error', function(err) {
	throw new Error('Redis client Error ' + err);
});

// create our job queue
var queue = kue.createQueue({
	prefix : config.kue_prefix,
	redis : {
		port : config.redis.master.port,
		host : config.redis.master.host,
		auth: config.redis.master.password,
		db: config.redis.master.db_index,
		options : {}
	},
	jobEvents : config.app_debug ? true : false // production环境建议关闭
});

queue.on('job enqueue', function(id, type) {
	//logger.log("server", "Job " + id + " got queued of type " + type);
}).on('job complete', function(id, result) {
	delJobStatus(null, id);
	if (config.redis.remove_task_complte) {
		kue.Job.get(id, function(err, job){
			if (err || !job) {
				logger.log("server", "removed completed job failed,no such job", {"job_id":id, "error":err});
				return;
			} 
			// 删除msg:task_job 的对应关系
		    try {
				var taskID = job.data.task_id;
			    redisClient.HDEL(config.redis.task_job, taskID, function (err, arg) {
			    	if (err) {
			    		logger.log("server", "removed completed task_job index failed", {"task_id":taskID, "task_type":job.data.task_type, "job_id":id, "error":err});
			    	}
		    	});
		    	job.remove(function(err){
			    	logger.log("server", "removed completed job", {"task_id":taskID, "task_type":job.data.task_type, "job_id":id, "error":err});
			    });
			} catch (err) {
				logger.log("error", "remove complete job failed", {"task_id":taskID, "task_type":job.data.task_type, "job_id":id, "error":err});
				return;
			}
		});
	}
}).on('job failed', function(id, result) {
	delJobStatus(null, id);
	kue.Job.get(id, function(err, job){
	    if (err || !job) {
	    	logger.log("server", "no such failed job", {"job_id":id, "error":err}); //job失败不执行callback url
	    	return;
	    }
	    var taskID = job.data.task_id;
	    try {
			logger.log("server", "job failed", {"task_id":taskID, "task_type":job.data.task_type, "job_id":id}); //job失败不执行callback url
	    	redisLogService.write(false, job.data);
	    	if (config.redis.remove_task_failed) {
	    		redisClient.multi()
				    .expire('kue:job:' + id + ':log', config.redis.ttl)
				    .expire('kue:job:' + id, config.redis.ttl)
				    .exec();
	    	}
		} catch (err) {
			logger.log("error", "set task failed status error", {"task_id":taskID, "task_type":job.data.task_type, "job_id":id, "error":err});
			return;
		}
	});
}).on('error', function(err) {
	logger.log("error", "job queue error", {"error":err});
});



process.once('uncaughtException', function(err) {
	logger.log("error", "uncaughtException", {"error":err});
})

//SIGTERM is not supported on Windows, it can be listened on. 
// linux command kill process will emmit this case
process.once('SIGTERM', function(sig) {
	queue.shutdown(3000, function(err) {
		logger.log("server", "Kue shutdown");
		process.exit(0);
	});
});

//SIGHUP is generated on Windows when the console window is closed, and on other 
//platforms under various similar conditions, see signal(7). 
// process.on('SIGHUP', function(arg) {
	// logger.log("server","kill process pid:" + arg);
// });


// 將所有在隊列中的job改成 active狀態
// queue.inactive( function( err, ids ) { // others are active, complete, failed, delayed
  // ids.forEach( function( id ) {
    // kue.Job.get( id, function( err, job ) {
      // job.active();
    // });
  // });
// });

function addReserveTask(taskID, plan, fn) {
	var taskData = {"task_id":taskID,
			"title":plan.task_type + "_pid_" + plan.task_id + "_" + taskID,
			"app_name":plan.app_name,
			"task_type":plan.task_type,
			"priority":plan.priority,"callbackurl":plan.callbackurl,
			"custom_data":plan.custom_data,"frequency":null,
			"plan_time":plan.plan_time,"commit_type":"once",
			"request_time":moment().format("YYYY-MM-DD H:mm:ss")};
	redisClient.sadd(config.redis.task_key, JSON.stringify(taskData), fn);
}

// increase taskid key
function incrTaskID(fn) {
	redisClient.incr(config.redis.taskids_key, fn); 
}

function parsePlan(cont) {
	redisClient.smembers(config.redis.plan_key, function (err, plans) {
		if(err){
			cont(err);
			return;
        }
        var times = plans.length,
			afterAll = _.after(times, cont); // 當for完成後執行
		plans.forEach(function (val) {
			var plan;
			try {
				plan = JSON.parse(val);
			} catch (err) {
				var info = "parse plan failed.err:" + err + ",plan:"+val;
				logger.log("error", "parse plan failed", {"plan":val, "error":err});
				cont(new Error(info));
				return;
			}
			
			if (plan.commit_type == "once" && (Date.now() >= (new Date(plan.plan_time).getTime()))) {
				redisClient.smove(config.redis.plan_key, config.redis.task_key, val, function(err, arg) {
					if (!arg) {
						logger.log("error", "move plan failed", {"plan_id": plan.task_id});
					}
				})
			} else if (plan.commit_type == "reserve") {
				parseReservePlan(plan);
			}
			afterAll();
		})
	}) 
}

function parseReservePlan(plan) {
	// 上一次執行時間
	var planID = plan.task_id;
	redisLogService.getLastReserveLog(planID, function (err, arg) {
		if (err) {
			logger.log("error", "get last reserve task log failed", {"plan_id":planID, "error":error});
			return;
		}
		var loopTime = frequencyTime(plan.frequency);
		if (!loopTime) {
			logger.log("error", "frequency time invalid", {"plan_id":planID, "frequency": plan.frequency});
			return false;
		}
		var now = new Date().getTime();
		var lastDoneTime = arg ? new Date(arg).getTime() : 0;
		var nextTime = lastDoneTime + loopTime;
		if (!arg || nextTime <= now) { 
			addPlanToTask(plan);
		} else {
			// waiting to do plan task
		}
	})
}

function addPlanToTask(plan) {
	var currTaskID;
	// 添加任務
	Thenjs(function (cont) {
		incrTaskID(function (err, arg) {
			if (err) {
				cont(new Error(err));
			} else {
				cont(null, arg);
			}
		});
	}).then(function (cont, taskID) {
		if (typeof taskID == "undefined" || taskID <= 0) {
			cont(new Error('incr task id failed!!'));
		}
		currTaskID = taskID;
		addReserveTask(taskID, plan, cont);
	}).then(function (cont, arg) {
		if (!arg) {
			cont(new Error("create task failed"));	
		} else {
			logger.log("server", "create reserve task success", {"task_id": currTaskID, "task_type":plan.task_type, "plan_id":plan.task_id});
			
			// 添加 執行時間log。不要以task的執行完成時間來記錄。會導致重複執行多次task
			redisLogService.addReserveLog(plan.task_id, currTaskID, function (err, arg) {
				cont(err);
			});
		}
	}).fail(function (cont, error) { // 通常应该在链的最后放置一个 `fail` 方法收集异常
		logger.log("error", "do reserve task error.", {"task_id": currTaskID, "task_type":plan.task_type, "plan_id":plan.task_id, "error":error});
	})
}

function createJob(cont) {
	redisClient.smembers(config.redis.task_key, function (err, tasks) {
		if(err){
			cont(err);
			return;
        }
        var times = tasks.length,
			afterAll = _.after(times, cont); // 當for完成後執行
		tasks.forEach(function (i) {
			var task;
			try {
				task = JSON.parse(i);
			} catch (err) {
				var info = "create job,parse data failed.err:" + err + ",task:" + i;
				logger.log("error", "create job,parse data failed", {"task":i, "error":err});
				cont(new Error(info));
				return false;
			}
			
			// 检查msg:task_job 是否已有taskid和jobid的对应关系，如有，则跳过，避免创建重复的job
			redisClient.hexists(config.task_job, task.task_id, function (err, result) {
				if (err) {
					cont(err);
					return;
				}
				
				if (result) return true;
				var job = queue.create(task.task_type, task)
					.priority(task.priority)
					.removeOnComplete(false)
					.attempts(config.retry_num)// 失败重试3次
					.backoff({delay: 60000, type:'fixed'}) //失敗1分鐘後重新執行，若失败的job重新執行成功 狀態改成成功
					.ttl(config.redis.activity_ttl * 1000) // 單位:毫秒,job在activity 状态下的等待时间，防止job无限停在activity，阻塞队列
					.searchKeys( ['task_id','title','app_name','task_type'])
					.save(function(err,arg) {
						if (err) {
							cont(new Error('create job failed!!task id:' + task.task_id));
						} else {
							removePendingTask(i, job.id);
						}
					});
			})
			afterAll();
		})
	})
}

// 創建任務成功後，將task從msg:task中刪除
function removePendingTask(taskLog, jobID) {
	var task;
	try {
		task = JSON.parse(taskLog);
	} catch (err) {
		logger.log("error", "remove pending task,parse task data failed", {"task":taskLog, "job_id":jobID, "error":err});
		return;
	}
	
	// remove from task
	redisClient.srem(config.redis.task_key, taskLog, function(err, arg) {
		if (err) {
			logger.log("error", "remove task failed", {"task_id": task.task_id, "task_type":task.task_type});
		}
		// 創建msg taskID 與kue job id 的對應關係表
		redisClient.hset(config.redis.task_job, task.task_id, jobID);
	});
}

function initSchedule() {
	// every second loop
	for (var i = 1; i < 60; i++) {
		times.push(i);
	}
	
	rule.second = times;
	logger.log("server", "start schedule");
	
	// 定時任務的schedule
	var c = 0;
	var j = schedule.scheduleJob(rule, function() {
		c++;
		// check plan
		Thenjs(function(cont) {
			redisClient.scard(config.redis.plan_key, cont);
		}).then(function(cont, len) {
			if (!len) {
				cont();
			} else {
				parsePlan(cont);
				// 注意這個cont的執行位置。是要在parsePlan之後執行，還是要同步執行？
				//cont();
			}
		}).then(function(cont, arg) {
			// 遍历 msg:task 创建任务
			createJob(cont);
		}).fail(function(cont, error) {
			logger.log("error", "task schedule error", {"error":error});
		});
	});

	// 按照优先级 执行 job 設置並發process
	for (var i in taskTypes) {
		var type = taskTypes[i],
		    concurrency = config[type + "_concurrency"] ? config[type + "_concurrency"] : 1;
		queue.process(type, concurrency, function(job, done) {
			setJobStatus(function (err, arg) {
				if (!arg || arg == null) {
					logger.log("server", "job locked", {"task_id":job.data.task_id, "task_type":job.data.task_type, "job_id":job.id});
				} else {
					commitJob(job, done);
				}
			}, job.id);
		});
	}
	
	function commitJob(job, done) {
		switch (job.type) {
			case "email":
				email(job, done);
				break;
			case "custom":
				customFunc(job, done);
				break;
			case "sms":
				sms(job, done);
				break;
			case "client_log":
				clientLog(job, done);
				break;
		}
	}
	
	queue.inactiveCount( function( err, total ) { // others are activeCount, completeCount, failedCount, delayedCount
	  logger.log("server","inactive jobs", {"count":total});
	});
	
	queue.activeCount( function( err, total ) { // others are activeCount, completeCount, failedCount, delayedCount
	  logger.log("server","active jobs",  {"count":total});
	});
	
	queue.active(function (err,ids) {
		ids.forEach( function( id ) {
		    kue.Job.get( id, function( err, job ) {
		      // Your application should check if job is a stuck one
		      job.inactive();
		      logger.log("server","restart active job", {"task_id":job.data.task_id, "task_type":job.data.task_type, "job_id":id});
		    });
	  	});
	})

	// 調用LUA處理一直不執行的task
	queue.watchStuckJobs(config.watchStuckJobsTime);
}

// 设置job id的锁状态，60秒有效
function setJobStatus(fn,jobID) {
	if (!config.distribute || !jobID) return true;
	redisClient.set(config.redis.job_lock_key + ":" + jobID, 1, "NX", "EX", 60, function (err, arg) {
		if (fn) fn(err, arg);
	});
}

function delJobStatus(fn,jobID) {
	if (!config.distribute || !jobID) return true;
	redisClient.del(config.redis.job_lock_key + ":" + jobID, function (err, arg) {
		if (fn) fn(err, arg);
	});
}

function frequencyTime(frequency) {
	if (!frequency) return 0;
	var intCount = frequency.replace(/[^\d]/g,'');
	var timeUnit = frequency.replace(/[^a-zA-Z]/g,'');
	var timeCount=0;
	if (!intCount || !timeUnit) return false;
	if (timeUnit == "m") { // minute
		timeCount = 1000 * 60;	
	}  else if (timeUnit == "h"){ // hour
		timeCount = 1000 * 60 * 60;
	} else if (timeUnit == "d"){ // day
		timeCount = 1000 * 60 * 60 * 24;
	}
	return intCount * timeCount;
}

function customFunc(job, done) {
	var task = job.data;
	customService.sendRequest(task, done);
}

function email(job, done) {
	var task = job.data;
	emailService.sendMail(task, done);
}

function sms(job, done) {
	var task = job.data;
	smsService.sendMessage(task, done);
}

function clientLog(job, done) {
	var task = job.data;
	if (!("custom_data" in task) || !task.custom_data || task.custom_data == "") {
		redisLogService.write(false, task);
		logger.log("error", "no custom data of client log task", {"task_id": task.task_id, "task_type": task.task_type});
		done(new Error('no custom data of task', {"task_id":task.task_id}));
		return;
	}
	try {
		var customData = JSON.parse(task.custom_data);
		logger.log("client_log", "client log", {"task_id": task.task_id, "task_type": task.task_type, "client_log": customData});
	} catch (err) {
		logger.log("error", "parse client log custom data failed", {"task_id":task.id, 
																	"task_type": task.task_type, 
																	"custom_data": task.custom_data, 
																	"error": err});
	} finally {
		done();
	}
}

function startProcess() {
	process.env.TZ = 'PRC';
	logger.log("server","message engine started!");
	process.setMaxListeners(100);
	initSchedule();
}

// start the UI
if (config.kue_website) {
	var app = express();
	kue.app.set('title', 'Message Center');
	app.use(authService);
	app.use(kue.app);
	app.listen(config.server_port, function (err) {
		if (err) {
			logger.log("error", 'message server start failed', {"error":err});
		} else {
			logger.log("server", 'message server started', {"port":config.server_port, "pid":process.pid});
		}
	});
}

startProcess();